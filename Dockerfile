FROM ubuntu:20.04
LABEL maintainer="Gavin Yap (gavinyap@42dev.com)"

# Update/Upgrade docker packages
RUN apt update 
RUN apt upgrade -y

RUN apt install mysql-server -y
RUN test -e /var/run/mysqld || install -m 755 -o mysql -g root -d /var/run/mysqld

RUN apt install vim sudo -y

COPY sudoers /etc/sudoers

COPY  init.sql /var/lib/mysql/init.sql

RUN  service mysql start && \
    mysql -u root mysql < /var/lib/mysql/init.sql

RUN mkdir -p /mysql && \
    chown mysql:root /mysql

COPY entryscript.sh /mysql

RUN chown mysql:root /mysql/entryscript.sh && chmod +x /mysql/entryscript.sh

WORKDIR /mysql

USER mysql

ENTRYPOINT [ "/mysql/entryscript.sh" ]